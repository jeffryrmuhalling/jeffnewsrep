//
//  UIImageExtension.swift
//  DailyFeed
//
//  Created by Jeffry Ronald Muhalling on 29/04/20.
//

import UIKit

extension UIImageView {

    func addGradient(_ color: [CGColor], locations: [NSNumber]) {

        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.superview!.frame
        gradient.colors = color
        gradient.locations = locations
        self.layer.addSublayer(gradient)
    }
}
